let fs = require("fs")
let {PATH_DATA} = require("../utils/constants")

let fsPromisse = fs.promises

class pessoaController {

 
    async getPessoas(req, res) {
        try {
                var data = await  this.jsonRead()
                res.send( data )
             } catch (err) {
                res.status(400).send(err.message)
        }
    }

    async jsonRead() {
        return await fsPromisse.readFile(PATH_DATA, "utf8")
    }


    async getPessoa(req, res) {
        try {
            let data = await this.jsonRead()
            data = this.searchPessoaInArray(req.body, data)
            console.log(data)
            res.send(data)
        } catch (err) {
            res.status(400).send(err.message)
        }
    }

    searchPessoaInArray(filter, pessoas) {
        return (JSON.parse(pessoas)).filter((pessoa) => {
           if (String(pessoa[filter.field]).startsWith(filter.query)) {
               return true
           }
        })
    }
}
            


module.exports =   new pessoaController();