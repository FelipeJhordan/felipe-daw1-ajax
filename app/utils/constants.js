let projectDir = __dirname.replace("app\\utils","")
module.exports = {
    DIR_PUBLIC: projectDir + "\\public\\",
    DIR_PAGES: projectDir  + "\\public\\pages\\",
    
    PATH_ROUTES: projectDir + "\\app\\routes\\index.js",
    PATH_PAGE_INDEX: projectDir + "\\public\\index.html",
    PATH_DATA: projectDir + "\\app\\data\\data.json",
}