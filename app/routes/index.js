var router = require("express").Router()
const constant = require("../utils/constants")
const pessoaController = require("../controller/pessoaController.js")

router.get("/", (req, res) => {
    res.sendFile(constant.PATH_PAGE_INDEX)
})

// Se não usar o bind perde-se o this e não permite chamar outra função da mesma classe.
router.post('/', pessoaController.getPessoas.bind(pessoaController));

router.post('/search', pessoaController.getPessoa.bind(pessoaController));

module.exports = router