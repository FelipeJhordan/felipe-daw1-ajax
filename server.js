const express = require ("express")
const {DIR_PUBLIC, PATH_ROUTES} = require("./app/utils/constants")
const routes = require(PATH_ROUTES)

const app = express()

app.use(express.static(DIR_PUBLIC))
app.use(express.json())
app.use(express.urlencoded( {extended: true} ))
app.use(routes)

app.listen(3000)